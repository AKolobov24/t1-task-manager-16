package ru.t1.akolobov.tm.api.controller;

public interface ICommandController {

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void displayInfo();

    void displayCommands();

    void displayArguments();

}
